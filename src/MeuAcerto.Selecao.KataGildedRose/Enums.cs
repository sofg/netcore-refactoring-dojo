namespace MeuAcerto.Selecao.KataGildedRose
{
    public enum TipoItem
    {
        Comum,
        Especial,
        Ingresso,
        Lendario,
        Conjurado
    }
}