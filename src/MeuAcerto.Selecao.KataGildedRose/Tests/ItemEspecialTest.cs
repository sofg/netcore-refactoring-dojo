﻿using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemEspecialTest
    {
        [Theory]
        [InlineData(3, 3)]
        public void AtualizarQualidade_AumentarQualidade(int prazoDeVenda, int qualidade)
        {
            Item item = new ItemEspecializado { Nome = "Especial", PrazoParaVenda = prazoDeVenda, Qualidade = qualidade, TipoItem = TipoItem.Especial };

            item.AtualizarQualidade();

            Assert.Equal(qualidade + 1, item.Qualidade);            
        }

        [Theory]
        [InlineData(2, 50)]
        public void AtualizarQualidade_AumentaQualidade_QualidadeNaoPodeSerMaiorQueCinquenta(int prazoParaVenda, int qualidade)
        {
            Item item = new ItemEspecializado { Nome = "Especial", PrazoParaVenda = prazoParaVenda, Qualidade = qualidade, TipoItem = TipoItem.Especial };

            item.AtualizarQualidade();

            Assert.Equal(50, item.Qualidade);
        }
    }
}
