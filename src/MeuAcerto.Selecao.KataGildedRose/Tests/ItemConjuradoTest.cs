﻿using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemConjuradoTest
    {
        [Theory]
        [InlineData(2, 4)]
        [InlineData(2, 6)]
        public void AtualizarQualidade_DiminuirQualidadeEmDois(int prazoParaVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Conjurado", PrazoParaVenda = prazoParaVenda, Qualidade = qualidade, TipoItem = TipoItem.Conjurado };

            item.AtualizarQualidade();

            Assert.Equal(qualidade - 2, item.Qualidade);
        }
    }
}
