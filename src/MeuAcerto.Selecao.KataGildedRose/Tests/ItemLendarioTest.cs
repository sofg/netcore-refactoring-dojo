﻿using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemLendarioTest
    {
        [Fact]
        public void AtualizarQualidade_PermanecerOitenta()
        {
            var item = new ItemEspecializado { Nome = "Lendario", PrazoParaVenda = 0, Qualidade = 0, TipoItem = TipoItem.Lendario };

            item.AtualizarQualidade();

            Assert.True(item.Qualidade == 80);
        }
    }
}
