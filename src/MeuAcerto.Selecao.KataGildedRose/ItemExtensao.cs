﻿namespace MeuAcerto.Selecao.KataGildedRose
{
    public static class ItemExtensao
    {
        const int qualidadeMinima = 0;
        const int qualidadeMaxima = 50;

        public static void AtualizarQualidade(this Item item)
        {
            if (item is ItemEspecializado)
            {
                var iEspecializado = (ItemEspecializado)item;

                switch (iEspecializado.TipoItem)
                {
                    case TipoItem.Especial:
                        AtualizarQualidadeEspecial(item);
                        break;
                    case TipoItem.Ingresso:
                        AtualizarQualidadeIngresso(item);
                        break;
                    case TipoItem.Lendario:
                        AtualizarQualidadeLendario(item);
                        break;
                    case TipoItem.Conjurado:
                        AtualizarQualidadeConjurado(item);
                        break;
                    default:
                        AtualizarQualidadeComum(item);
                        break;
                }
            }
            else
            {
                AtualizarQualidadeComum(item);
            }
        }

        private static void AtualizarQualidadeComum(Item item)
        {
            if (item.PrazoParaVenda <= 0 && item.Qualidade >= 2)
            {
                item.Qualidade -= 2;
            }
            else if (item.PrazoParaVenda > 0 && item.Qualidade > 0)
            {
                item.Qualidade -= 1;
            }

            if (item.Qualidade < 0)
            {
                item.Qualidade = qualidadeMinima;
            }

            AtualizarPrazoParaVenda(item);
        }

        private static void AtualizarQualidadeEspecial(Item item)
        {
            if (item.PrazoParaVenda > 0)
            {
                item.Qualidade++;
            }
            else
            {
                item.Qualidade += 2;
            }

            if (item.Qualidade > qualidadeMaxima)
            {
                item.Qualidade = qualidadeMaxima;
            }

            AtualizarPrazoParaVenda(item);
        }

        private static void AtualizarQualidadeLendario(Item item)
        {
            item.Qualidade = 80;
        }

        private static void AtualizarQualidadeConjurado(Item item)
        {
            item.Qualidade -= 2;

            if (item.Qualidade < qualidadeMinima)
            {
                item.Qualidade = qualidadeMinima;
            }

            AtualizarPrazoParaVenda(item);
        }

        private static void AtualizarQualidadeIngresso(Item item)
        {
            if (item.PrazoParaVenda <= 0)
            {
                item.Qualidade = 0;
            }
            else if (item.PrazoParaVenda <= 5)
            {
                item.Qualidade += 3;
            }
            else if (item.PrazoParaVenda <= 10)
            {
                item.Qualidade += 2;
            }
            else
            {
                item.Qualidade++;
            }

            if (item.Qualidade > qualidadeMaxima)
            {
                item.Qualidade = qualidadeMaxima;
            }

            AtualizarPrazoParaVenda(item);
        }

        private static void AtualizarPrazoParaVenda(Item item)
        {
            item.PrazoParaVenda--;
        }
    }
}
