﻿namespace MeuAcerto.Selecao.KataGildedRose
{
    public class ItemEspecializado : Item
    {
        public TipoItem TipoItem { get; set; } = TipoItem.Comum;
    }
}
