﻿using System;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
	class Program
	{
		public static void Main(string[] args)
		{
			IList<Item> itens = new List<Item>{
				new ItemEspecializado {Nome = "Corselete +5 DEX", PrazoParaVenda = 10, Qualidade = 20, TipoItem = TipoItem.Comum },
				new ItemEspecializado {Nome = "Queijo Brie Envelhecido", PrazoParaVenda = 2, Qualidade = 0, TipoItem = TipoItem.Especial },
				new ItemEspecializado {Nome = "Elixir do Mangusto", PrazoParaVenda = 5, Qualidade = 7, TipoItem = TipoItem.Comum },
				new ItemEspecializado {Nome = "Dente do Tarrasque", PrazoParaVenda = 0, Qualidade = 80, TipoItem = TipoItem.Lendario },
				new ItemEspecializado {Nome = "Dente do Tarrasque", PrazoParaVenda = -1, Qualidade = 80, TipoItem = TipoItem.Lendario },
				new ItemEspecializado
				{
					Nome = "Ingressos para o concerto do Turisas",
					PrazoParaVenda = 15,
					Qualidade = 20, 
					TipoItem = TipoItem.Ingresso
				},
				new ItemEspecializado
				{
					Nome = "Ingressos para o concerto do Turisas",
					PrazoParaVenda = 10,
					Qualidade = 49, 
					TipoItem = TipoItem.Ingresso
				},
				new ItemEspecializado
				{
					Nome = "Ingressos para o concerto do Turisas",
					PrazoParaVenda = 5,
					Qualidade = 49, 
					TipoItem = TipoItem.Ingresso
				},				
				new ItemEspecializado {Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 3, Qualidade = 6, TipoItem = TipoItem.Conjurado }
			};

			var app = new GildedRose(itens);


			for (var i = 0; i < 31; i++)
			{
				Console.WriteLine("-------- dia " + i + " --------");
				Console.WriteLine("Nome, PrazoParaVenda, Qualidade");
				for (var j = 0; j < itens.Count; j++)
				{
					Console.WriteLine(itens[j].Nome + ", " + itens[j].PrazoParaVenda + ", " + itens[j].Qualidade);
				}
				Console.WriteLine("");
				app.AtualizarQualidade();
			}
		}

	}
}
